import React from 'react';
import Typography from '@material-ui/core/Typography';

const About = () => 

        <div>
          <div style={styles.title}>
            <Typography variant="display3" align="center" color="textPrimary" gutterBottom>
                Refill Not Landfill
            </Typography>
            <Typography variant="title" align="center" color="textSecondary" paragraph>
                Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt,
                explicabo.
            </Typography>
          </div>

          <div style={styles.textwrapper}>
            <div style={styles.leftText}>
              <Typography variant="title" align="center" color="textSecondary" paragraph>
                  Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                  totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt,
                  explicabo.
              </Typography>
              <Typography variant="title" align="center" color="textSecondary" paragraph>
                  Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                  totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt,
                  explicabo.
              </Typography>
              <Typography variant="title" align="center" color="textSecondary" paragraph>
                  Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                  totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt,
                  explicabo.
              </Typography>
            </div>

            <div style={styles.rightText}>
              <Typography variant="title" align="center" color="textSecondary" paragraph>
                  Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                  totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt,
                  explicabo.
              </Typography>
              <Typography variant="title" align="center" color="textSecondary" paragraph>
                  Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                  totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt,
                  explicabo.
              </Typography>
              <Typography variant="title" align="center" color="textSecondary" paragraph>
                  Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                  totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt,
                  explicabo.
              </Typography>
            </div>
          </div>
        </div>



const styles = {
  title: {
    paddingTop: 10,
    paddingBottom: 5,
    maxWidth: 900,
    margin: '0 auto',
  },
  textwrapper: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: 10,
    height: 450,
  },
  leftText: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    maxWidth: 600,
  },
  rightText: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    maxWidth: 600,
  },
};

export default About;
