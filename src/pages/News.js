import React from 'react';

import Typography from '@material-ui/core/Typography';

import { TwitterTimelineEmbed } from 'react-twitter-embed';
import FacebookProvider, { Page } from 'react-facebook';

class News extends React.Component {
  render() {
    return (
      <div>
        <div style={styles.newswrap}>
          <div style={styles.twitterdiv}>
            <TwitterTimelineEmbed
                sourceType="profile"
                screenName="greengobletltd"
                options={{ height: 750 }}
                noHeader='True'
                noBorders='True'
                noFooter='True'
                noScrollbar=''
              />
          </div>
          <div style={styles.textdiv}>
            <Typography variant="display3" align="center" color="textPrimary" gutterBottom>
                What Have We Been Doing
            </Typography>
            <Typography variant="title" align="center" color="textSecondary" paragraph>
                Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt,
                explicabo.
            </Typography>
          </div>
          <div>
            <FacebookProvider appId="Green Goblet Ltd" >
              <Page
                href="https://www.facebook.com/greengobletltd/"
                tabs="timeline"
                width='700'
                height='745'
                showFacepile="false" />
            </FacebookProvider>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  newswrap: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: 50,
    width: '100%',
  },
  twitterdiv: {
    width: 500,
    height: 750,
  },
  textdiv: {
    width: 500,
  },
};

export default News;
