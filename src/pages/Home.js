import React from 'react';

import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

import {CustomCard} from '../Components'

function HomePage() {
  return (
        <div>
          <div style={styles.header}>
            <Typography variant="display3" align="center" color="textPrimary" gutterBottom>
                Green Goblet
            </Typography>
            <Typography variant="title" align="center" color="textSecondary" paragraph>
                Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt,
                explicabo.
            </Typography>
          </div>
          <div style={styles.cardwrapper}>
            <Link to="/About" style={styles.links}>
              <CustomCard
              style={styles.cards}
              heading='About Us'
              cardText='We are Green Goblet and this is all about us'
              image={require('../Images/6.gif')}
            />
            </Link>
            <Link to="/News" style={styles.links}>
              <CustomCard
              style={styles.cards}
              heading='News and Events'
              cardText='This is what Green Goblet have been doing'
              image={require('../Images/maxresdefault.jpg')}
            />
            </Link>
            <Link to="/Products" style={styles.links}>
              <CustomCard            
              heading='Products'
              cardText='This is what Green Goblet can offer you'
              image={require('../Images/Web-version-festival.gif')}
            />
            </Link>
          </div>
        </div>
  );
};

const styles = {
  links: {
    textDecoration: 'none',
  },
  header: {    
    paddingTop: 10,
    maxWidth: 900,
    margin: '0 auto',
  },
  cardwrapper: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: 100,
    marginBottom: 100,
  },
};

export default HomePage;
