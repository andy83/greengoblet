import React from 'react';

import Typography from '@material-ui/core/Typography';

import { CustomCard, EnquireForm, } from '../Components';

class Products extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      products: {
        PC: false, 
        HP: false,
        SG: false,
      }
      
    }
  }

  submitRequest = () => console.log(this.state.products)

  render() {
    return (
      <div style={styles.rootdiv}>        
        <div style={styles.header}>
          <Typography variant="title" align="center" color="textSecondary" gutterBottom>
              Please select the products you require and complete the form for a quote
          </Typography>
        </div>

        <div style={styles.cardwrapper}>          
          <CustomCard
          style={styles.cards}
          heading='Plastic Pint Cups'
          cardText='Photographic or Printed Finish'
          image={require('../Images/IML2.JPG')}          
          onClick={() => this.setState( {products: {...this.state.products, PC: !this.state.products.PC}}  )}
          />
          <CustomCard
          style={styles.cards}
          heading='Plastic Half Pint Cups'
          cardText='Printed Finish'
          image={require('../Images/HalfPint200.JPG')}
          onClick={() => this.setState( {products: {...this.state.products, HP: !this.state.products.HP}} )}
          />
          <CustomCard
          style={styles.cards}
          heading='Plastic Shot Glasses'
          cardText='Printed Finish'
          image={require('../Images/shot.jpg')}
          onClick={() => this.setState( {products: {...this.state.products, SG: !this.state.products.SG}} )}
          />                                  
        </div>
        <EnquireForm
          products={this.state.products}
        />
      </div>
    );
  };
};
  

const styles = {
  
  links: {
    textDecoration: 'none',
    color: 'black',
  },
  header: {
    flexDirection: 'column',
    paddingTop: 10,
    maxWidth: 900,
    margin: '0 auto',
  },
  cardwrapper: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: 50,
    marginBottom: 50,
  },
  buttonWrapper: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 20,
  }
};

export default Products;
