import React, { Component } from 'react';
import {Header, Footer} from './Layout';

import image1 from './Images/XCekRx.jpg';

export default class App extends Component {

  render() {
    return (
      <div style={ styles.rootdiv }>
        <Header />
        <Footer />
      </div>    
    );
  }
}

const styles = {
  rootdiv: {
    backgroundImage: `url(${image1})`,
    height: '100vh',
  },
}