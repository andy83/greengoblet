import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

function CustomCard(props) {
  const { classes } = props;
  return (
    <Card 
    className={classes.card} 
    onClick={props.onClick}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={props.image}
        />
        <CardContent>
          <Typography gutterBottom variant="headline" component="h2">
            {props.heading}
          </Typography>
          <Typography component="p">
            {props.cardText}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

CustomCard.propTypes = {
  classes: PropTypes.object,
  heading: PropTypes.string,
  cardText: PropTypes.string,
};

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '60%',
  },
};

export default withStyles(styles)(CustomCard);
