import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';



class CustomButton extends React.Component {
  
  render() {
    return (
      <div>      
        <Button 
          variant="contained" 
          color="primary"
          onClick={this.props.onClick}>
          {this.props.buttonText}
        </Button>      
      </div>
    );
  }
  
}

CustomButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

export default withStyles(styles)(CustomButton);