import React from 'react';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';

import {
  Link,
} from 'react-router-dom';

const CustomMenu = () =>

  <div >
    <Typography color="inherit" style={ styles.navtext }>
      <MenuItem><Link to="/" style={styles.links}>Home</Link></MenuItem>
      <MenuItem><Link to="/About" style={styles.links}>About</Link></MenuItem>
      <MenuItem><Link to="/News" style={styles.links}>News</Link></MenuItem>
      <MenuItem><Link to="/Products" style={styles.links}>Products</Link></MenuItem>
    </Typography>
  </div>


export default CustomMenu;

const styles = {
  links: {
    textDecoration: 'none',
    color: 'black',
    paddingLeft: 80,
    paddingRight: 80,
  },
  navtext: {
    display: 'flex',
    justifyContent: 'space-evenly',
  },

};
