import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import CustomMenu from '../Components/CustomMenu';

const CustomNavBar = () =>    
  
  <div>
    <AppBar position="static" style={styles.bar}>
      <Toolbar>
        <CustomMenu />
      </Toolbar>
    </AppBar>
  </div>

const styles = {
  bar: {
    background: 'transparent', 
    boxShadow: 'none',
    justifyContent: 'space-around'
  } 
};

export default CustomNavBar;
