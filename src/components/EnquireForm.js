import React, { Component } from 'react'

export class Enquire extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
      Name: '',
      Email: '',
      Phone: '',
      ComEve: '',
      Date: '',
      HearAbout: '',
      Message: '',      
    }
  }

  handleChange = (event, field) => {    
    this.setState({[field]: event.target.value});
  }

  handleSubmit = (event) => {
        fetch('/email', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          Name: this.state.Name,
          Email: this.state.Email,
          Phone: this.state.Phone,
          ComEve: this.state.ComEve,
          Date: this.state.Date,
          HearAbout: this.state.HearAbout,
          Message: this.state.Message,
          Products: this.state.products,
          PC: this.props.products.PC, 
          HP: this.props.products.HP,
          SG: this.props.products.SG,
        }),
      });
    event.preventDefault();    
  }

  render() {
    return (
      <div>
        <div style={styles.formWrapper}>         
            <form onSubmit={this.handleSubmit}>
              <label>
                Name:
                <input type="text" value={this.state.value} onChange={event => this.handleChange(event, 'Name')}  />            
              </label>
              <label>
                Email:
                <input type="text" value={this.state.value} onChange={event => this.handleChange(event, 'Email')}  />            
              </label>
              <label>
                Phone Number:
                <input type="text" value={this.state.value} onChange={event => this.handleChange(event, 'Phone')}  />            
              </label>
              <label>
                Company or Event:
                <input type="text" value={this.state.value} onChange={event => this.handleChange(event, 'ComEve')}  />            
              </label>
              <label>
                Delivery Date:
                <input type="text" value={this.state.value} onChange={event => this.handleChange(event, 'Date')}  />            
              </label>
              <label>
                How did you hear about us:
                <input type="text" value={this.state.value} onChange={event => this.handleChange(event, 'HearAbout')}  />            
              </label>
              <label>
                Message:
                <input type="text" value={this.state.value} onChange={event => this.handleChange(event, 'Message')}  />            
              </label>  
              <input type="submit" value="Enquire" />
            </form>
            <div style={styles.buttonWrapper}>
            </div> 
          
        </div>        
      </div>
    )
  }
}

const styles = {
  formWrapper: {
    
  }
}

export default Enquire
