import React, { Fragment } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';

import {Home, About, News, Products, } from '../Pages';
import CustomNavBar from '../Components/CustomNavBar';


const Header = () =>

    <Router>
      <div>
        <div style={ styles.bardiv }>
          <img
          alt={'Green Goblet Logo'}
          style={styles.logo}
          src={ require('../Images/Green_Goblet_Logo_Black.png') } />
          <CustomNavBar /> 
        </div>         
        <Fragment>
          <Route exact path="/" component={Home}/>
          <Route path="/About" component={About}/>
          <Route path="/News" component={News}/>
          <Route path="/Products" component={Products}/>          
        </Fragment> 
      </div>
    </Router>  

export default Header;

const styles = {
  bardiv: {
    display: 'flex',
    alignItems: 'center',
  },
  logo: {
    padding: 20,
  },
};