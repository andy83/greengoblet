import React from 'react';
import Typography from '@material-ui/core/Typography';

 const Footer = () =>

    <div style={styles.footer}>          
      <Typography variant="caption" color="inherit">
        <div style={styles.lines}>
            Address: Unit 1a Warlow Industrial Estate, Commerce Way, Highbridge, Somerset, TA9 4AG 
          <br></br>
        </div>
          <ul style={styles.lines}>
            <li style={styles.items}>Phone: Sales - +44 (0) 1172 440 103,</li>
            <li style={styles.items}>Logistics - +44 (0) 7926 33 85 22,</li>
            <li style={styles.items}>Accounts - +44 (0) 07547 756 957,</li>
            <li style={styles.items}>E-mail: info@green-goblet.com</li>
          </ul>   
          <ul style={styles.lines}>
            <li style={styles.items}>Company reg number: 08306172 </li>
            <li style={styles.items}>Company vat number: 157508396</li>
          </ul>  
      </Typography>
    </div>

export default Footer;

const styles = {
  footer: {
    paddingTop: 20
  },
  lines: {    
    display: 'flex',
    justifyContent: 'center',
    listStyleType: 'none', 
  },
  items: {
    paddingRight: 15,
  },
};